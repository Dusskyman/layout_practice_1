import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:layout_practice_1/custom_widgets/custom_appbar.dart';
import 'package:layout_practice_1/custom_widgets/custom_carousel_slider.dart';
import 'package:layout_practice_1/layouts/main_layout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MainPage());
  }
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: CustomAppbar(),
      bottomBar: CustomCarouselSlider(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [],
      ),
    );
  }
}
