import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class CustomCarouselSlider extends StatefulWidget {
  @override
  _CustomCarouselSliderState createState() => _CustomCarouselSliderState();
}

class _CustomCarouselSliderState extends State<CustomCarouselSlider> {
  int currentPage = 0;
  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
      itemCount: 5,
      options: CarouselOptions(
          onPageChanged: (index, reason) {
            setState(() {
              currentPage = index;
            });
          },
          height: 125,
          aspectRatio: 2 / 3,
          viewportFraction: 0.21),
      itemBuilder: (context, index, realIndex) => Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Card(
            elevation: 2,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: Colors.black38,
            child: SizedBox(
              height: 100,
              width: 75,
              child: Icon(
                Icons.android_outlined,
                size: 40,
                color: index == currentPage ? Colors.yellow[700] : Colors.grey,
              ),
            ),
          ),
          AnimatedOpacity(
            opacity: index == currentPage ? 1 : 0,
            duration: Duration(milliseconds: 500),
            child: Container(
              width: 35,
              height: 10,
              decoration: BoxDecoration(
                  border: Border.all(),
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.yellow),
            ),
          ),
        ],
      ),
    );
  }
}
