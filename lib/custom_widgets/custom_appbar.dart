import 'package:flutter/material.dart';

class CustomAppbar extends StatelessWidget {
  final Widget title;
  final Widget tralling;

  const CustomAppbar({
    this.title,
    this.tralling,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.width * 0.04,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              title ??
                  Text(
                    'Custom AppBar',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
              tralling ??
                  Container(
                    alignment: Alignment.bottomRight,
                    width: 20,
                    height: 10,
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                          width: 2,
                        ),
                      ),
                    ),
                    child: Container(
                      width: 12,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            width: 2,
                          ),
                        ),
                      ),
                    ),
                  ),
            ],
          ),
          Expanded(
            child: SizedBox(),
          )
        ],
      ),
      decoration: BoxDecoration(
        border: Border.symmetric(
          horizontal: BorderSide(width: 2, color: Colors.black),
        ),
      ),
    );
  }
}
