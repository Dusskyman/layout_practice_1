import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  final Widget appBar;
  final Widget child;
  final Widget bottomBar;
  MainLayout({
    this.appBar,
    this.child,
    this.bottomBar,
  });
  @override
  Widget build(BuildContext context) {
    var querryWidth = MediaQuery.of(context).size.width;
    var querryHeight = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Material(
        color: Color.fromRGBO(244, 234, 221, 1),
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: SizedBox(
            width: querryWidth + querryWidth * 0.2,
            height: querryHeight,
            child: Stack(
              children: [
                Positioned(
                  right: -querryWidth * 0.3,
                  top: -querryWidth * 0.1,
                  child: Container(
                    width: querryWidth * 0.8,
                    height: querryWidth * 0.8,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(233, 93, 85, 1),
                    ),
                  ),
                ),
                Positioned(
                  right: querryWidth * 0.1,
                  top: querryWidth * 0.25,
                  child: Container(
                    width: querryWidth * 0.5,
                    height: querryWidth * 0.5,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        width: 2,
                        color: Color.fromRGBO(233, 93, 85, 1),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  right: -querryWidth * 0.15,
                  top: querryWidth * 0.33,
                  child: Container(
                    width: querryWidth * 0.3,
                    height: querryWidth * 0.3,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 2, color: Colors.white),
                    ),
                  ),
                ),
                Positioned(
                  left: -querryWidth * 0.15,
                  bottom: -querryWidth * 0.12,
                  child: Container(
                    width: querryWidth * 0.35,
                    height: querryWidth * 0.35,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(233, 93, 85, 1),
                    ),
                  ),
                ),
                Positioned(
                  left: querryWidth * 0.01,
                  bottom: -querryWidth * 0.17,
                  child: Container(
                    width: querryWidth * 0.3,
                    height: querryWidth * 0.3,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        width: 2,
                        color: Color.fromRGBO(233, 93, 85, 1),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: -querryWidth * 0.07,
                  bottom: querryWidth * 0.15,
                  child: Container(
                    width: querryWidth * 0.15,
                    height: querryWidth * 0.15,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 2, color: Colors.white),
                    ),
                  ),
                ),
                Positioned(
                    top: querryWidth * 0.1,
                    left: querryWidth * 0.1,
                    width: querryWidth * 0.8,
                    height: querryHeight * 0.1,
                    child: appBar ?? SizedBox()),
                Positioned(
                    bottom: 80,
                    left: 0,
                    right: 0,
                    child: bottomBar ?? SizedBox()),
                Padding(
                  padding: EdgeInsets.only(bottom: 180),
                  child: child ?? SizedBox(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
